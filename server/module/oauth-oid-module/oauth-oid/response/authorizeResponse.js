export default async function (auth_req_id, expires_in, interval, state) {

    return {
        auth_req_id: auth_req_id,
        expires_in: expires_in,
        interval: interval,
        state: state
    }


}