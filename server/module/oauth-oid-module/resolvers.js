import {authorizationHandler, tokenHandler,  revokeHandler, introspectHandler } from "./oauth-oid";

const { query } = require('nact');

module.exports = {

    Mutation: {


        authorize: async (obj, args, ctx, info) => {

            return authorizationHandler(args.input, ctx);

        },

        token: async (obj, args, ctx, info) => {

            return tokenHandler(args.input, ctx);

        },

        revoke: async (obj, args, ctx, info) => {

            return revokeHandler(args.input,ctx);

        }
    },
    Query:{


        introspect: async (obj, args, ctx, info) => {

            return introspectHandler(args.input,ctx);

        },
    }
}