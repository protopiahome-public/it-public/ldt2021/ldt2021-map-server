import eventCollection from "./eventCollection";

const { spawnStateless} = require('nact');

export default function(serverActor){
	spawnStateless(serverActor, eventCollection, "events");
}
