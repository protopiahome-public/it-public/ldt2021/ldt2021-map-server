import placeCollection from "./placeCollection";

const { spawnStateless} = require('nact');

export default function(serverActor){
	spawnStateless(serverActor, placeCollection, "places");
}
