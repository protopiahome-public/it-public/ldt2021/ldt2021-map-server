import {associateHandler} from "./associate";

const { query } = require('nact');

module.exports = {

    Mutation: {

        associate: async (obj, args, ctx, info) => {

            return associateHandler(args.input, ctx);

        },

    },
    Query:{

    }
}