const {AuthenticationError, ForbiddenError} = require('apollo-server');
// import {AuthenticationError} from 'apollo-server';
import {ObjectId} from "promised-mongo";

const { query } = require('nact');

const resource = "place";
const fs = require('fs');

module.exports = {

    Mutation:{
        uploadFile: async (obj, args, context, info) => {

			if (args.input.file_upload) {
				let file = await args.input.file_upload.then(file => {
					let filename = new ObjectId();
					file.createReadStream().pipe(fs.createWriteStream(__dirname + "/../../../upload/" + filename))
					file.original_filename = file.filename
					file.filename = filename
					return file;
				});
				delete file.createReadStream;
				args.input.file = "/upload/" + file.filename;
				delete args.input.file_upload;
			}
        },
    },

    Query: {
		
	} 
}