

// https://github.com/Urigo/graphql-modules/blob/master/docs/introduction/context.md

import {GraphQLModule} from "@graphql-modules/core";

import {authenticateHandler} from "./oauth";
import db from "../../db";
import getQueryName from "./oauth/helpers/getQueryName";

export default function(ctx) {

    //async
    return  new GraphQLModule({
        name: "MainModule",
        async context({  req, h }, ) {

            const query_name = getQueryName(req);

            let authenticate;
            //TODO костыль так как импорт модулей происходит на удаленном сервере иначе чем на локальном
			
            if(!(query_name === "authorize" || query_name === "token" || query_name === "__schema")){

                authenticate = await authenticateHandler(req, h, ctx);
                console.log("authenticate");
                console.log(authenticate);

                //await


                return {
                    ...ctx,
                    db: db,
                    user: authenticate.user,
                    client: authenticate.client,
                    // sid: authenticate
                }
            }else{
                return {
                    ...ctx,
                    db: db
                    // sid: authenticate
                }
            }



        },
    });

}