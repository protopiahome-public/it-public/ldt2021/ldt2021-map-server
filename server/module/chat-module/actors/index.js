import chatCollection from "./chatCollection";

const { spawnStateless} = require('nact');

export default function(serverActor){
	spawnStateless(serverActor, chatCollection, "chats");
}
