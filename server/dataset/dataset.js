var file_users = cat("users.json");
var file_clients = cat("clients.json");

var users = JSON.parse(file_users);
var clients = JSON.parse(file_clients);

users.forEach( (user) => {

    user.register_time = new Date();
    db.user.insertOne(user)

});

clients.forEach( (client) => {

    db.client.insertOne(client)

});
