const axios = require("axios");
const config = require(__dirname + "/../server/config/config");
import db from "../server/db";
import {ObjectId} from "promised-mongo";

(async function() {
    let places = await (db.place.find());
    places.forEach(async place => {
        let geocode = await axios.get("https://geocode-maps.yandex.ru/1.x/?apikey=" + config.yandex_key + "&geocode=" + place.longitude + "," + place.latitude + "&format=json");
        geocode = geocode.data;
        //console.log(JSON.stringify(geocode, null, 4));
        //console.log(geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components)
        let components = geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components;
        let geo_analytics = {};
        components.forEach((component) => {
            let key = component.kind;
            let value = component.name;
            if (geo_analytics[key]) {
                key = key + "2";
            }
            geo_analytics[key] = value;
        });
        //console.log(geo_analytics);
        await db.place.update({_id: new ObjectId(place._id)}, {$set: {geo_analytics: geo_analytics}});
    });

    let users = await (db.user.find());
    users.forEach(async user => {
        if (!user.address) {
            return;
        }
        let geocode = await axios.get("https://geocode-maps.yandex.ru/1.x/?apikey=" + config.yandex_key + "&geocode=" + encodeURIComponent(user.address) + "&format=json");
        geocode = geocode.data;
        //console.log ("https://geocode-maps.yandex.ru/1.x/?apikey=" + config.yandex_key + "&geocode=" + encodeURIComponent(user.address) + "&format=json");
        //console.log(JSON.stringify(geocode, null, 4));
        //console.log(geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components)
        let components = geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components;
        let geo_analytics = {};
        components.forEach((component) => {
            let key = component.kind;
            let value = component.name;
            if (geo_analytics[key]) {
                key = key + "2";
            }
            geo_analytics[key] = value;
        });
        //console.log(geo_analytics);
        await db.user.update({_id: new ObjectId(user._id)}, {$set: {geo_analytics: geo_analytics}});
    });
})()
